const {remote} = require('electron');
const {Menu, MenuItem} = remote;
var mouseX = 0;
var mouseY = 0;
var img_link = '';
var target_link = '';

var currentTabIndex = $('.active').index()
var webview = $('.webview').find('webview')[currentTabIndex]

//Menu 1
let menu1 = new Menu();
menu1.append(new MenuItem({ label: 'Back', type: 'normal', enabled: false, click () { goBack() } }));
menu1.append(new MenuItem({ label: 'Forward', type: 'normal', enabled: false, click() { goForward() } }));
menu1.append(new MenuItem({ label: 'Reload', type: 'normal', accelerator: process.platform === 'darwin' ? 'Command+R' : 'Ctrl+R', click() { doRefresh() } }));
menu1.append(new MenuItem({ type: 'separator' }));
menu1.append(new MenuItem({ label: 'Add to bookmark', type: 'normal' }));
menu1.append(new MenuItem({ label: 'Save as...', type: 'normal', accelerator: process.platform === 'darwin' ? 'Command+S' : 'Ctrl+S', click() { savePageAs() } }));
menu1.append(new MenuItem({ label: 'Print...', type: 'normal', accelerator: process.platform === 'darwin' ? 'Command+P' : 'Ctrl+P' }));
menu1.append(new MenuItem({ label: 'Translate to Vietnamese', type: 'normal' }));
menu1.append(new MenuItem({ type: 'separator' }));
menu1.append(new MenuItem({ label: 'View source page', type: 'normal', accelerator: process.platform === 'darwin' ? 'Command+U' : 'Ctrl+U', click() { viewPageSource() } }));
menu1.append(new MenuItem({ label: 'Inspect element', accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I', click () { openDevTools(mouseX, mouseY) } }));

//Menu 2
let menu2 = new Menu();
menu2.append(new MenuItem({ label: 'Open link in new tab', type: 'normal', click() { openInNewTab() }}));
menu2.append(new MenuItem({ label: 'Open link in new window', type: 'normal', click() { openInNewWindow() } }));
menu2.append(new MenuItem({ label: 'Open link in private window', type: 'normal'}));
menu2.append(new MenuItem({ type: 'separator' }));
menu2.append(new MenuItem({ label: 'Save link as...', type: 'normal', click() { savePageAs(obj.url) } }));
menu2.append(new MenuItem({ label: 'Copy link address', type: 'normal', click() { copyLinkAddress() } }));
menu2.append(new MenuItem({ type: 'separator' }));
menu2.append(new MenuItem({ label: 'Inspect element', accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I', click () { openDevTools(mouseX, mouseY) } }));

//Menu 3
let menu3 = new Menu();
menu3.append(new MenuItem({ label: 'Open link in new tab', type: 'normal', click() { openInNewTab() }}));
menu3.append(new MenuItem({ label: 'Open link in new window', type: 'normal'}));
menu3.append(new MenuItem({ label: 'Open link in private window', type: 'normal'}));
menu3.append(new MenuItem({ type: 'separator' }));
menu3.append(new MenuItem({ label: 'Save link as...', type: 'normal'}));
menu3.append(new MenuItem({ label: 'Copy link address', type: 'normal', click() { copyLinkAddress() } }));
menu3.append(new MenuItem({ type: 'separator' }));
menu3.append(new MenuItem({ label: 'Open image in new tab', type: 'normal', click() { openImageInNewTab() }}));
menu3.append(new MenuItem({ label: 'Save image as', type: 'normal'}));
menu3.append(new MenuItem({ label: 'Copy image address', type: 'normal', click() { copyImageAddress() } }));
menu3.append(new MenuItem({ type: 'separator' }));
menu3.append(new MenuItem({ label: 'Inspect element', accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I', click () { openDevTools(mouseX, mouseY) } }));

webview.addEventListener('update-target-url', function(e){
  target_link = e.url;
})

webview.addEventListener('contextmenu', function (e) {
  // e.preventDefault();
  mouseX = e.x;
  mouseY = e.y;

  webview.getWebContents().executeJavaScript("document.addEventListener('contextmenu', (e) => {if (e.target.localName != 'video'){localStorage.setItem('target', e.target.currentSrc);}else{localStorage.setItem('target', 'video');} }, false);", true, function(result) {

  })
  webview.getWebContents().executeJavaScript("setTimeout(()=>{localStorage.removeItem('target')},1000); result = localStorage.getItem('target');", true, function(result) {
    setTimeout(function () {
      if (result !== 'undefined' && result !== null){
        img_link = result;
        if (img_link != 'video'){
          menu3.popup(remote.getCurrentWindow());
        }
      }else {
        if (target_link != ''){
          menu2.popup(remote.getCurrentWindow());
        }else {
          menu1.popup(remote.getCurrentWindow());
        }
      }
    }, 100);
  })
}, false);





function createContextMenu() {
  // const {remote} = require('electron');
  // const {Menu, MenuItem} = remote;
  // var mouseX = 0;
  // var mouseY = 0;
  // var obj = {};

  var currentTabIndex = $('.active').index()
  var webview = $('.webview').find('webview')[currentTabIndex]

  //Menu 1
  let menu1 = new Menu();
  menu1.append(new MenuItem({ label: 'Back', type: 'normal', enabled: false, click () { goBack() } }));
  menu1.append(new MenuItem({ label: 'Forward', type: 'normal', enabled: false, click() { goForward() } }));
  menu1.append(new MenuItem({ label: 'Reload', type: 'normal', accelerator: process.platform === 'darwin' ? 'Command+R' : 'Ctrl+R', click() { doRefresh() } }));
  menu1.append(new MenuItem({ type: 'separator' }));
  menu1.append(new MenuItem({ label: 'Add to bookmark', type: 'normal' }));
  menu1.append(new MenuItem({ label: 'Save as...', type: 'normal', accelerator: process.platform === 'darwin' ? 'Command+S' : 'Ctrl+S', click() { savePageAs() } }));
  menu1.append(new MenuItem({ label: 'Print...', type: 'normal', accelerator: process.platform === 'darwin' ? 'Command+P' : 'Ctrl+P' }));
  menu1.append(new MenuItem({ label: 'Translate to Vietnamese', type: 'normal' }));
  menu1.append(new MenuItem({ type: 'separator' }));
  menu1.append(new MenuItem({ label: 'View source page', type: 'normal', accelerator: process.platform === 'darwin' ? 'Command+U' : 'Ctrl+U', click() { viewPageSource() } }));
  menu1.append(new MenuItem({ label: 'Inspect element', accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I', click () { openDevTools(mouseX, mouseY) } }));

  //Menu 2
  let menu2 = new Menu();
  menu2.append(new MenuItem({ label: 'Open link in new tab', type: 'normal', click() { openInNewTab() }}));
  menu2.append(new MenuItem({ label: 'Open link in new window', type: 'normal', click() { openInNewWindow() } }));
  menu2.append(new MenuItem({ label: 'Open link in private window', type: 'normal'}));
  menu2.append(new MenuItem({ type: 'separator' }));
  menu2.append(new MenuItem({ label: 'Save link as...', type: 'normal', click() { savePageAs(obj.url) } }));
  menu2.append(new MenuItem({ label: 'Copy link address', type: 'normal', click() { copyLinkAddress() } }));
  menu2.append(new MenuItem({ type: 'separator' }));
  menu2.append(new MenuItem({ label: 'Inspect element', accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I', click () { openDevTools(mouseX, mouseY) } }));

  //Menu 3
  let menu3 = new Menu();
  menu3.append(new MenuItem({ label: 'Open link in new tab', type: 'normal', click() { openInNewTab() }}));
  menu3.append(new MenuItem({ label: 'Open link in new window', type: 'normal'}));
  menu3.append(new MenuItem({ label: 'Open link in private window', type: 'normal'}));
  menu3.append(new MenuItem({ type: 'separator' }));
  menu3.append(new MenuItem({ label: 'Save link as...', type: 'normal'}));
  menu3.append(new MenuItem({ label: 'Copy link address', type: 'normal', click() { copyLinkAddress() } }));
  menu3.append(new MenuItem({ type: 'separator' }));
  menu3.append(new MenuItem({ label: 'Open image in new tab', type: 'normal', click() { openImageInNewTab() }}));
  menu3.append(new MenuItem({ label: 'Save image as', type: 'normal'}));
  menu3.append(new MenuItem({ label: 'Copy image address', type: 'normal', click() { copyImageAddress() } }));
  menu3.append(new MenuItem({ type: 'separator' }));
  menu3.append(new MenuItem({ label: 'Inspect element', accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I', click () { openDevTools(mouseX, mouseY) } }));

  webview.addEventListener('update-target-url', function(e){
    target_link = e.url;
  })

  webview.addEventListener('contextmenu', function (e) {
    // e.preventDefault();
    mouseX = e.x;
    mouseY = e.y;

    webview.getWebContents().executeJavaScript("document.addEventListener('contextmenu', (e) => { localStorage.setItem('target', e.target.currentSrc) }, false);", true, function(result) {

    })
    webview.getWebContents().executeJavaScript("setTimeout(()=>{localStorage.removeItem('target')},1000); result = localStorage.getItem('target');", true, function(result) {
      setTimeout(function () {
        if (result !== 'undefined'){
          img_link = result;
          menu3.popup(remote.getCurrentWindow());
        }else {
          if (target_link != ''){
            menu2.popup(remote.getCurrentWindow());
          }else {
            menu1.popup(remote.getCurrentWindow());
          }
        }
      }, 100);
    })
  }, false);
}
