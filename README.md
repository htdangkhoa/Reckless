# **Reckless Browser - Web browser for speed, simplicity, and security**  
Reckless is a web browser using Node JS, Javascript, Html and Css base on Electron framework. Reckless will help you have a great browsing  
experience because it has basic features of a web browser and some special features just for Reckless to help you surf the Web at high speeds.  
In addition, your information will also be to ensure safety. Now, you go to experience and let us know your comments to us more and more  
complete.  
<ENTER>
<ENTER>    
Thank you!!!
  
## **Features**  
- Surf web with multiple tabs.  
- Open multiple windows.
- Drag tab.  
- One click to clear all tabs.  
- Develop tools.  

## **Downloads**  
Coming soon.  

## **Installation**  
Requires [Node.js](https://nodejs.org/) v4+ to run.
```sh
$ npm install -g electron
```

## **Development**
Step 1: Create directory
```sh
$ mkdir Reckless
$ cd Reckless
```
Step 2: Clone project from git
```sh
$ git clone https://gitlab.com/huynhtran.dangkhoa/Reckless.git
```
Step 3: Run project
```sh
$ electron .
``` 

## **Building**  
```sh
$ npm install -g electron-packager
$ electron-packager <sourcedir> <appname> --platform=<platform> --out <your-directory> --version 1.0.0

ex: $ electron-packager ./ Reckless --all --out ~/Desktop/Reckless-build --version 1.0.0
```

### **Screenshot**  
![Screenshotfrom2016-12-1200-35-57.png](http://sv1.upsieutoc.com/2016/12/12/Screenshotfrom2016-12-1200-35-57.png)  
![Screenshotfrom2016-12-1200-36-06.png](http://sv1.upsieutoc.com/2016/12/12/Screenshotfrom2016-12-1200-36-06.png)  
![vid_25_11_2016_at_18-23-24.gif](http://sv1.upsieutoc.com/2016/11/26/vid_25_11_2016_at_18-23-24.gif)