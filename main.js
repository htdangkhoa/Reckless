const {app, BrowserWindow, Menu, ipcMain} = require('electron')
const path = require('path')
const url = require('url')

let win

app.on('ready', () => {
  var os = require('os');
  if (os.platform().toLowerCase().indexOf('win') != -1){
    win = new BrowserWindow({
      width: 1200,
      height: 800,
      minWidth: 1200,
      minHeight: 800,
      frame: false
    });
  }else {
    win = new BrowserWindow({
      width: 1200,
      height: 800,
      minWidth: 1200,
      minHeight: 800
    });
  }

  ipcMain.on('synchronous-message', (event, arg) => {
    console.log(arg)  // prints "ping"
    event.returnValue = 'pong'
  })

  win.maximize();
  // win.setMenu(null);
  win.webContents.openDevTools();

  win.loadURL(url.format({
    pathname: path.join(__dirname, '/views/index.html'),
    protocol: 'file:',
    slashes: true
  }));

  // menu = Menu.buildFromTemplate(application_menu);
  // Menu.setApplicationMenu(menu);

  win.webContents.session.on('will-download', (event, item, webContents) => {
    // Set the save path, making Electron not to prompt a save dialog.
    // item.setSavePath('/tmp/save.pdf')

    item.on('updated', (e, state) => {
      if (state === 'interrupted') {
        console.log('Download is interrupted but can be resumed')
      } else if (state === 'progressing') {
        if (item.isPaused()) {
          console.log('Download is paused')
        } else {
          console.log("getSavePath: ", item.getSavePath());
          console.log("getURL: ", item.getURL());
          console.log("getMimeType: ", item.getMimeType());
          console.log("getFilename: ", item.getFilename());
          console.log("getContentDisposition: ", item.getContentDisposition());
          console.log("getURLChain: ", item.getURLChain());
          console.log("getLastModifiedTime: ", item.getLastModifiedTime());
          console.log("getETag: ", item.getETag());
          console.log("getStartTime: ", item.getStartTime());

          console.log(item.getReceivedBytes() + '/' + item.getTotalBytes())
        }
      }
      // console.log('state updated: ', state);
      // console.log(item.getReceivedBytes());
    })
    item.once('done', (event, state) => {
      if (state === 'completed') {
        console.log('Download successfully')
      } else {
        console.log('Download failed: ', state)
      }
      // if (state === 'cancelled'){
      //
      // }

      // console.log('state done: ', state);

    })
  })

  win.on('closed', () => {
    win = null
  });
})
